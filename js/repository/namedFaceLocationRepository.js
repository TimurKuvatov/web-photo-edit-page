let namedFaceLocationRepository =[]
let currentName;
function saveNamedFaceLocation(namedFaceLocation,prevName){
    const prevEl = namedFaceLocationRepository.find(el=>el.name===prevName);
    if (prevEl){
        prevEl.name = namedFaceLocation.name;
        prevEl.faceLocation = namedFaceLocation.faceLocation;
        return false;
    }
    namedFaceLocationRepository.push(namedFaceLocation);
    return true;
}

function getNamedFaceLocation(name){
    return namedFaceLocationRepository.find(el=>el.name===name);
}

function deleteNamedFaceLocation(name){
     namedFaceLocationRepository = namedFaceLocationRepository.filter(location => location.name !== name);
}

