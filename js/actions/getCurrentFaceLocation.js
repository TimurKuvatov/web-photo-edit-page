function getCurrentFaceLocation() {
    const cropBoxData = cropper.getCropBoxData();
    return new FaceLocation(
        Math.max(Math.min(cropData.y, originalHeight), 0),
        Math.max(Math.min((cropData.x + cropData.width), originalWidth), 0),
        Math.max(Math.min((cropData.y + cropData.height), originalHeight), 0),
        Math.max(Math.min(cropData.x, originalWidth), 0)
    );
}