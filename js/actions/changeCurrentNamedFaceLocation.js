currentName = document.getElementById("current_name");

function changeCurrentNamedFaceLocation(faceLocationHTML) {
    prevName = faceLocationHTML.querySelector('.name-in-list').innerText;
    currentName.value = prevName;
    const namedFaceLocation = getNamedFaceLocation(prevName);
    const faceLocationCrop = namedFaceLocation.faceLocation;
    console.log(faceLocationCrop);
    cropper.setData(
        {
            x: faceLocationCrop.left,
            y: faceLocationCrop.top,
            width: faceLocationCrop.right - faceLocationCrop.left,
            height: faceLocationCrop.bottom - faceLocationCrop.top
        }
    )
}