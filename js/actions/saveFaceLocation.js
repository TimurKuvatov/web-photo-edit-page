saveButton = document.querySelector('.saveButton');
locationsWrapper = document.querySelector('.locations-wrapper');
let prevName = ""
saveButton.onclick = function () {
    if (currentName.value === "") {
        alert("Пожалуйста, введите имя!")
    } else {
        const currentFaceLocation = getCurrentFaceLocation();
        const namedFaceLocation = new NamedFaceLocation(currentName.value, currentFaceLocation);
        const isNotAlreadyExisted = saveNamedFaceLocation(namedFaceLocation, prevName);
        if (isNotAlreadyExisted) {
            const faceLocationHTML = fromStringHTML(namedFaceLocationTemplate(namedFaceLocation.name));
            faceLocationHTML.addEventListener('click', () => {
                changeCurrentNamedFaceLocation(faceLocationHTML);
            });
            faceLocationHTML.querySelector('.location .cross-container img').addEventListener('click', (event) => {
                event.preventDefault();
                event.stopPropagation();
                deleteNamedFaceLocation(event.target.parentElement.parentElement.querySelector('.name-in-list').innerText);
                event.target.parentElement.parentElement.remove();
            });
            locationsWrapper.insertBefore(faceLocationHTML, locationsWrapper.firstChild);
        } else {
            const elements = document.querySelectorAll('.name-in-list');
            for (const element of elements) {
                if (element.innerText.trim() === prevName) {
                    element.innerText = namedFaceLocation.name;
                    break;
                }
            }
        }
        clearNameField();
    }
}