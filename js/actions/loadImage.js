const image = document.getElementById("image");
let originalWidth;
let originalHeight;
let cropper;
let cropData = {};

async function loadImage() {
    const imageSource = (await loadImageFromS3(photo_key)).toString();
    image.src = imageSource;
    let newImg = new Image();
    newImg.onload = function () {
        originalHeight = newImg.height;
        originalWidth = newImg.width;
    }
    newImg.src = imageSource;
    let options = {
        dargMode: "move",
        viewMode: 2,
        modal: false,
        background: false
    }
    cropper = new Cropper(image, {
        crop(event) {
            cropData = {
                x: event.detail.x,
                y: event.detail.y,
                width: event.detail.width,
                height: event.detail.height,
            };
        },
    });
    document.querySelector('.zoomIn').onclick = () => cropper.zoom(0.1)
    document.querySelector('.zoomOut').onclick = () => cropper.zoom(-0.1)
}

loadImage();


