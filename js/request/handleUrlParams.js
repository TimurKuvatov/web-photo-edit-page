const url = new URL(window.location.href);
const params = url.searchParams;

const photo_key = params.get('key');



const API_TOKEN = params.get('API_TOKEN');
const API_BASE_URL = params.get('API_BASE_URL');
const API_RECOGNITION_PREFIX =params.get('API_RECOGNITION_PREFIX');
const S3_KEY_ID = params.get('S3_KEY_ID');
const S3_ACCESS_KEY = params.get('S3_ACCESS_KEY');
const S3_REGION_NAME = params.get('S3_REGION_NAME');
const S3_ENDPOINT = params.get('S3_ENDPOINT');
const S3_BASKET_NAME = params.get('S3_BASKET_NAME');