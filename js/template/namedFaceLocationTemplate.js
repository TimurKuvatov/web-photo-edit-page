function namedFaceLocationTemplate(name) {
    return `<div class="location">
        <div class="name-in-list">
             ${name}
        </div>
        <div class="cross-container">
            <img src="../../images/cross.png" alt="Image"/>
        </div>
    </div>`;
}