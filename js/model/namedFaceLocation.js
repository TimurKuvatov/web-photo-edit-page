class NamedFaceLocation {
  constructor(name, faceLocation) {
    this.name = name;
    this.faceLocation = faceLocation;
  }

  toJSON() {
        return {
            name: this.name,
            faceLocation: this.faceLocation.toJSON()
        };
    }
}