class FaceLocation {
  constructor(top, right, bottom, left) {
    this.top = top;
    this.right = right;
    this.bottom = bottom;
    this.left = left;
  }

   toJSON() {
        return {
            top: Math.floor(this.top),
            right: Math.floor(this.right),
            bottom: Math.floor(this.bottom),
            left: Math.floor(this.left)
        };
    }
}